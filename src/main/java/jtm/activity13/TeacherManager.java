package jtm.activity13;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn = null; {
	
	try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		System.out.println("Connecting to database");
		conn = DriverManager.getConnection("jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root", "");
		System.out.println("Connected to database successfully");
	} catch (Exception e) {
		 System.err.println(e);
	}
	 }

	public TeacherManager() {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// user = "root"
		// pass = ""
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods
	}

	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 */
	public Teacher findTeacher(int id) {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"
		
		Teacher teacher = new Teacher();
		
		try{
			String query = "SELECT * FROM testJava.test_table WHERE id = 0";
	    Statement st = conn.createStatement();
	    ResultSet result = st.executeQuery(query);
		while (result.next())
	      {
	        int ids = result.getInt("id");
	       /* String firstName = rs.getString("first_name");
	        String lastName = rs.getString("last_name");
	        Date dateCreated = rs.getDate("date_created");
	        boolean isAdmin = rs.getBoolean("is_admin");
	        int numPoints = rs.getInt("num_points");*/
	      }
		st.close();
    }
    catch (Exception e)
    {
      System.err.println("Got an exception! ");
      System.err.println(e.getMessage());
    }
		return null;
	}

	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List<Teacher> teacherList = new ArrayList<Teacher>();
		return null;

	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.
	boolean status = false;
	
		try {
			PreparedStatement s = conn.prepareStatement("INSERT INTO database_activity.Teacher (firstname, lastname) VALUES (?,?)");
			stmt.setString(1, firstName);
			stmt.setString(2, lastName);
			stmt.executeUpdate() {
			if (count > 0) 
			    conn.commit();
			    return true;
			    } else {
			    conn.rollback();
			     return false;
			    }
		}

	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		
		try {
			String firstName = null;
			String lastName = null;
			int id = 0;
			Teacher teacher1 = new Teacher(id, firstName, lastName);

			String sqlInsert = "INSERT INTO database_activity.Teacher (firstname, lastname) VALUES (?, ?)";
			PreparedStatement prepStatement = conn.prepareStatement(sqlInsert);
			prepStatement.setString(1, firstName);
			prepStatement.setString(2, lastName);
			prepStatement.executeUpdate();
			conn.commit();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		boolean status = false;
		// TODO #6 Write an sql statement that updates teacher information.
		String firstName = null;
		String lastName = null;
		int id = 0;
		
		try {
			conn.setAutoCommit(false);
			PreparedStatement s = conn.prepareStatement("UPDATE database_activity.Teacher" + "Set firstname = ? AND lastname = ?");
			s.setInt(3, teacher.getId());
			s.setString(1, teacher.getFirstName());
			s.setString(2, teacher.getLastName());
			if (s.executeUpdate() > 0) {
				conn.commit();
				return true;
			} else
				return false;		

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		try {
			conn.setAutoCommit(false);
			PreparedStatement st = conn.prepareStatement("DELETE FROM database_activity.Teacher where id-?");
			st.setInt(1, id);
			st.executeUpdate();
			conn.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void closeConnecion() {
		// TODO Close connection to the database server and reset conn object to null
		try {
			if (conn != null)
				conn.close();
			conn = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
