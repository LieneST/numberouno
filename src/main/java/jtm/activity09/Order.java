package jtm.activity09;

import java.util.Objects;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection 
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */
//Dummy changes to check

public class Order implements Comparable<Order>{
	String customer; // Name of the customer
	String name; // Name of the requested item
	int count; // Count of the requested items
	
	public Order(String orderer, String intemName, Integer count) {
		this.customer = orderer;
		this.name = intemName;
		this.count = count;
	}
	
	@Override
	public int compareTo(Order o) {
		
		int tempItemName = 0;
		int tempCustomer = 0;
		int tempCount = 0;
		
		if(this.name == null && o.name == null) {
			tempItemName = 0;
			} else if(this.name == null) {
			tempItemName = -1;
			} else if (o.name == null) {
				tempItemName = 1;
			} else {
				tempItemName = this.name.compareTo(o.name);
			}
		
		if (tempItemName == 0) {
			if(this.customer == null && o.customer == null) {
				tempCustomer = 0;
			} else if(this.customer == null) {
				tempCustomer = -1;
			}else if(o.customer == null) {
				tempCustomer = 1;
			} else {
				tempCustomer = this.customer.compareTo(o.customer);
			}
			
		}
	
	if(tempItemName == 0 && tempCustomer == 0) {
		if(this.count == 0 && o.count == 0 ) {
			tempCount = 0;
		} else if (this.count == 0) {
			tempCount = -1;
		} else if (o.count == 0) {
			tempCount = 1;
		} else {
			tempCount = this.count - o.count;
		}
	}
	
	if (tempItemName !=0) {
		return tempItemName;
	} else if (tempCustomer != 0) {
		return tempCustomer;
	} else 
		return tempCount;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(count, customer, name);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof Order)) {
			return false;
		}
		Order other = (Order) obj;
		
		return count == other.count && Objects.equals(customer, other.customer) && Objects.equals(name, other.name);
		
	}
	
	@Override
	public String toString() {
		String message = this.name + ": " + this.customer + ": " + this.count;
		return message;
	}
	
	public static void main(String[] args) {
	Order myOrder = new Order("Nele", "unicorn", 4);
	Order myOrder = new Order("Kristaps", "console", 1);
	Order myOrder = new Order("Made", "bike", 2);
	System.out.println(myOrder.compareTo(hisOrder));
	
	if (myOrder.compareTo(hisOrder) == 0)
		System.out.println("equals");
	else if (myOrder.compareTo(hisOrder) > 0)
		System.out.println("MyOrder is greater");
	else 
		System.out.println("MyOrder is smaller");

	
	}
}
