package jtm.activity09;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

//import com.gargoylesoftware.htmlunit.javascript.host.Map;

/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */

public class Orders {
	
	List hisList = new ArrayList();
	
	/*-
	 * TODO #1
	 * Create data structure to hold:
	 *   1. some kind of collection of Orders (e.g. some List)
	 *   2. index to the current order for iterations through the Orders in Orders
	 *   Hints:
	 *   1. you can use your own implementation or rely on .iterator() of the List
	 *   2. when constructing list of orders, set number of current order to -1
	 *      (which is usual approach when working with iterateable collections).
	 */


//example:
		
public static void main(String[] args) {
	
	List<String> hisList = new ArrayList<String>();
	hisList.add(new String("crisps"));
	hisList.add(new String("apples"));
	hisList.add(new String("water"));
	hisList.sort(Comparator.naturalOrder());
		
	Iterator<String> iterator = hisList.iterator();
	
	Order previous = null;
	int count = 1;
	
	if (hisList.size() == 0)
		return;
	
	for (Order book : hisList) {
		
		if (previous == null) {
			previous = book;
			continue;
		}
		
		if (previous.getCategory() == book.getCategory()) {
			count += 1;
			continue;
		}
		
			categories.put(previous.getCategory(), count);
			count = 1;
			previous = book;
		}
	
	categories.put(previous.getCategory(), count);
	System.out.println(categories);
}
}

	class Order implements Comparable<Order> {
		
		public String category;		
		public String author;
		public String name;
		public int yearPublished = 0;
		
		@Override
		public int compareTo(Order book) {
			return this.getCategory().compareTo(book.getCategory());
		}
		
		
		public Order( String category, String author, String name, int year) {
			this.category = category;
			this.author = author;
			this.name = name;
			this.yearPublished = year;
}
		public String getCategory() {
			return this.category;
		}	
		
		@Override
		public String toString() {
			return this.author + this.name;
		}
		
	
}