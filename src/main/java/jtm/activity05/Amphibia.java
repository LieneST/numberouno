package jtm.activity05;

import jtm.activity04.Road;
import jtm.activity04.Transport;

public class Amphibia extends Transport {
	private Ship ship;
	private Vehicle vehicle;

	public Amphibia(String id, float consumption, int tankSize, byte sails, int wheels) {
		super(id, consumption, tankSize);
		ship = new Ship(id, sails);
		vehicle = new Vehicle (id, consumption, tankSize, wheels);
		// TODO Auto-generated constructor stub
		
	}
	 @Override
	public String move(Road road) {
	if (road instanceof WaterRoad)
		return getId() + " " + getType() + " is sailing on " + ship.toString() + " with " + ship.sails + " sails";			
	if (road.getClass() == Road.class)
		return this.getId() + " " + this.getType() + " is driving on " + road.getFrom() + " — " + road.getTo() + ", " + road.getDistance() + "km" + " with " + vehicle.wheels + " wheels";
	return null;
	}
	
	public static void main(String[] args) {
		Amphibia testAmp = new Amphibia("1", 2.50f, 20, (byte) 4, 5);
				System.out.println(testAmp);
	}
	
}
